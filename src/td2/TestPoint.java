package td2;

public class TestPoint {

	public static void main(String[] args) {
		
		Point p;
		
		p=new Point(7,2);
		
		System.out.println("x=" + p.getX());
		
		System.out.println("y=" + p.getY());
		
		System.out.println("Changement des coordonnes..."); /*Exercice1.4*/
		
		p.setX(5);
		
		p.setY(0);
		
		System.out.println("x=" + p.x); 
		
		System.out.println("y=" + p.y);
		
		System.out.println("x=" + p.getX());
		
		System.out.println("y=" + p.getY());
		
		
		System.out.println("On dplace le point.."); /*Exercice1.8*/
		
		p.deplacer(6, 3);
		
		System.out.println("x=" + p.getX() + " et y=" + p.getY());
		
	}

}
